
Scientific [poster](2016/ISPPD/poster.pdf) presented at the [International Symposium On Pneumococci & Pneumococcal Diseases](https://isppd2016.kenes.com) 2016.

## Poster

![Poster](poster.png)

## References

de Steenhuijsen Piters _et al._ [Dysbiosis of upper respiratory tract microbiota in elderly pneumonia patients](https://www.nature.com/articles/ismej201599). _ISME J_. 2015.
