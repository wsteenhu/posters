
This repository provides additional details to the scientific [poster](2021/IHMC/poster.pdf) presented at [International Human Microbiome Consortium Congress](https://ihmc2021.com) 2021 (see below).

## Full author list

Wouter A.A. de Steenhuijsen Piters <a href="https://orcid.org/0000-0002-1144-4067" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>1,2</sup>, Rebecca Watson<sup>3</sup>, Emma M. de Koff<sup>1,2,4</sup>, Raiza Hasrat<sup>1,2</sup>, Kayleigh Arp<sup>1,2</sup>, Mei Ling J.N. Chu<sup>1,2</sup>, Pieter-Kees C. M. de Groot<sup>5</sup>, Marlies A. Van Houten<sup>4</sup>, Elisabeth A.M. Sanders<sup>2</sup> and [Debby Bogaert](https://www.ed.ac.uk/inflammation-research/people/principal-investigators/professor-debby-bogaert) <a href="https://orcid.org/0000-0003-2651-7000" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>1-3§</sup>

<sup>§</sup> Corresponding author.

## Affiliations

<sup>1</sup> Department of Paediatric Immunology and Infectious Diseases, Wilhelmina Children’s Hospital/University Medical Center Utrecht, Utrecht, The Netherlands.  
<sup>2</sup> National Institute for Public Health and the Environment, Bilthoven, The Netherlands.  
<sup>3</sup> University of Edinburgh Centre for Inflammation Research, Queen's Medical Research Institute, University of Edinburgh, Edinburgh, United Kingdom.  
<sup>4</sup> Spaarne Gasthuis Academy, Hoofddorp and Haarlem, The Netherlands.
<sup>5</sup> Department of Obstetrics and Gynaecology, Spaarne Gasthuis, Hoofddorp, the Netherlands.  
<sup>5</sup> Spaarne Gasthuis Academy, Hoofddorp and Haarlem, The Netherlands.

## Poster

![Poster](poster.png)

## Abstract

**Introduction.** Respiratory tract infections (RTIs) impose a major burden on global health in infants. Aberrant development of the respiratory (nasopharyngeal) microbiota composition directly following birth has been associated with higher RTI susceptibility. We hypothesize that differences in microbiome development in early-life may imprint local mucosal immunity, explaining differences in subsequent RTI susceptibility. 

**Methods.** We longitudinally investigated gene expression profiles (microarray), microbiota (16S-rRNA sequencing) and respiratory viruses (qPCR) in nasopharyngeal samples from birth until 12 months of life in 114 infants (up to 11 visits/subject).

**Results.** We found strongest dynamics in gene expression profiles within the first days of life, mostly involving Toll-like receptor (TLR)- and inflammasome signalling, which coincided with rapid microbial niche differentiation. Early, asymptomatic viral infection triggered interferon gene module activity, which in turn was related microbiota dynamics. Particularly, premature transition from a health-associated _Corynebacterium_/_Dolosigranulum_- to a _Moraxella_-dominated profile or early _Haemophilus_ enrichment were related with a higher number of subsequent (viral) RTIs over the first year of life. Using a multi-omic approach, a.o. showing that commensal _Dolosigranulum_ is related to a neutrophil-dampening host phenotype, whereas _Corynebacterium_ is associated with a reduced antiviral response, suggesting complementary species-specific host-microbial effects.

**Conclusions.** Together, we show that early viral infection may provoke a sequence of events, likely altering respiratory microbial profiles through early-life host immune responses, impacting RTI susceptibility as a result of ongoing host-microbial crosstalk. 

<img src="figures/conclusion_MUIS_trx.png" alt="conclusion" width="600"/>

**Figure 1.** Theoretical model summarising host-microbial interactions determining RTI susceptibility.

## Corresponding manuscript

The manuscript was published in [_Nat microbiol_](https://www.nature.com/articles/s41564-021-01043-2).

## Related literature

This project includes samples from the Microbiome Utrecht Infant Study (MUIS). In this study, we longitudinally included infants, collecting samples at regular time intervals from birth on.

The Bogaert lab published four articles on the MUIS-cohort (see overview below) and at the moment of writing few are in preparation.

First author(s) | Year | Title | Journal
--- | --- | --- | ---
Reyman _et al._ | 2019 | [Impact of delivery mode-associated gut microbiota dynamics on health in the first year of life](https://www.nature.com/articles/s41467-019-13014-7) | _Nat Comm_
Man & Clerc _et al._ | 2019 | [Loss of Microbial Topography between Oral and Nasopharyngeal Microbiota and Development of Respiratory Infections Early in Life](https://www.atsjournals.org/doi/full/10.1164/rccm.201810-1993OC) | _Am J Resp Crit Care Med_
Bosch & de Steenhuijsen Piters _et al._ | 2017 | [Maturation of the Infant Respiratory Microbiota, Environmental Drivers, and Health Consequences. A Prospective Cohort Study](https://www.atsjournals.org/doi/full/10.1164/rccm.201703-0554OC) | _Am J Resp Crit Care Med_
Bosch _et al._ | 2016 | [Development of Upper Respiratory Tract Microbiota in Infancy is Affected by Mode of Delivery](https://www.thelancet.com/journals/ebiom/article/PIIS2352-3964(16)30225-0/fulltext) | _EBioMedicine_

## Contact

* Wouter A.A. de Steenhuijsen Piters, Ph.D.
    * Email: [w.a.a.desteenhuijsenpiters@umcutrecht.nl](mailto:w.a.a.desteenhuijsenpiters@umcutrecht.nl)
    * Institution: University Medical Center Utrecht/National Institute for Public Health and the Environment
    * Department: Paediatric Infectious Disease and Immunology
    * Lab: Bogaert Lab; PI: Professor Debby Bogaert; [d.bogaert@ed.ac.uk](mailto:d.bogaert@ed.ac.uk)

## Acknowledgements

This poster is part of the [#betterposter](https://twitter.com/hashtag/betterposter?src=hashtag_click) movement started by [Mike Morrison](https://twitter.com/mikemorrison?lang=en).
