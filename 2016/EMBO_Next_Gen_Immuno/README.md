
Scientific [poster](2016/EMBO_Next_Gen_Immuno/poster.pdf) presented at [EMBO Next Generation Immunology](https://www.weizmann.ac.il/conferences/NGI2016/welcome) 2016.

At this conferece, this poster was awarded the first poster prize.

The same poster was also presented at the [International Symposium On Pneumococci & Pneumococcal Diseases](https://isppd2016.kenes.com) 2016. 

## Poster

![Poster](poster.png)

## References

de Steenhuijsen Piters & Heinonen _et al._ [Nasopharyngeal Microbiota, Host Transcriptome, and Disease Severity in Children with Respiratory Syncytial Virus Infection](https://www.atsjournals.org/doi/full/10.1164/rccm.201602-0220OC). _Am J Resp Crit Care Med_. 2016.