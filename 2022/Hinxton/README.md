
This repository provides additional details to the scientific [poster](2022/Hinxton/poster.pdf) presented at [Exploring Human Host-Microbiome Interactions in Health and Disease](https://coursesandconferences.wellcomeconnectingscience.org/event/microbiome-interactions-in-health-and-disease-20221024/) 2022 (see below).

## Full author list

[Debby Bogaert](https://www.ed.ac.uk/inflammation-research/people/principal-investigators/professor-debby-bogaert)<a href="https://orcid.org/0000-0003-2651-7000" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>1,2§</sup>, Gina J. van Beveren<sup>1,3</sup>, Emma M. de Koff<a href="https://orcid.org/0000-0002-7888-1229" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>1,3</sup>, Paula Lusarreta Parga<sup>2</sup>, Carlos E. Balcazar Lopez<sup>2</sup>, Lilian Koppensteiner<a href="https://orcid.org/0000-0002-7986-7453" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>2</sup>, Melanie Clerc<sup>2,3</sup>, Raiza Hasrat<sup>1,4</sup>, Kayleigh Arp<sup>1,4</sup>, Mei Ling J.N. Chu<sup>1,4</sup>, Pieter C. M. de Groot<sup>5</sup>, Elisabeth A.M. Sanders<a href="https://orcid.org/0000-0002-6672-3266" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>1,2</sup>, Marlies A. Van Houten<a href="https://orcid.org/0000-0002-5942-8082" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>6</sup> and Wouter A.A. de Steenhuijsen Piters<a href="https://orcid.org/0000-0002-1144-4067" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>1,4§</sup>

<sup>§</sup> Co-corresponding authors.

## Affiliations

<sup>1</sup> Department of Paediatric Immunology and Infectious Diseases, Wilhelmina Children’s Hospital/University Medical Center Utrecht, Utrecht, The Netherlands.  
<sup>2</sup> Centre for Inflammation Research, Queen's Medical Research Institute, University of Edinburgh, Edinburgh, United Kingdom.  
<sup>3</sup> Institute of Microbiology, ETH Zürich, Zürich, Switzerland (current address).  
<sup>4</sup> Centre for Infectious Disease Control, National Institute for Public Health and the Environment, Bilthoven, The Netherlands.  
<sup>5</sup> Department of Obstetrics and Gynaecology, Spaarne Gasthuis, Hoofddorp and Haarlem, The Netherlands.  
<sup>6</sup> Spaarne Gasthuis Academy, Hoofddorp and Haarlem, The Netherlands.  

## Poster

![Poster](poster.png)

## Abstract

Early-life microbiota seeding and subsequent development is crucial to future health. Caesarean-section (CS) birth, as opposed to vaginal delivery, affects early mother-to-infant transmission of microbes. We hypothesize that mother-to-infant transmission of microbes occurs across a broad range of body sites or niches. Using 16S-rRNA-sequencing we assessed mother-to-infant microbiota seeding and early-life microbiota development across six maternal (breastmilk, vagina, gut, oropharynx, nasopharynx and skin) and four infant body sites (gut, oropharynx, nasopharynx and skin) over the first month of life in 120 mother-infant pairs (_n_ = 2,457 samples). 
Across all infants, we estimated that on average 58.5% of the infant microbiota composition could be attributed to any of the maternal source communities. The highest maternal contribution to the infant microbiota was observed for breastmilk (mean 31.6%), followed by maternal skin (mean 25.7%), saliva (mean 18.6%), nasopharynx (mean 9.4%), faeces (mean 4.1%) and vaginal microbiota (mean 3.5%). All maternal communities seeded multiple infant niches. We identified both shared and niche-specific host and environmental factors shaping the infant microbiota, for example infant faecal and nasopharyngeal microbiota were affected by mode of delivery more than infant saliva and skin microbiota. In CS-born infants, we found reduced seeding of the infant faecal microbiota by maternal faecal microbes, whereas colonization with breastmilk microbiota was increased when compared to vaginally born infants. This suggests that lack of microbial transfer by one niche may be filled in by another. Despite, we observed disrupted mother-infant transmission of health-associated bacteria including Bifidobacterium spp., which was related to lack of persistent colonization of health-promoting Bacteroides spp. in CS-born infants. Skin-skin transmission events, which occur relatively frequently, were used to mine for ‘heritable’ microbes. Intriguingly, these microbes were enriched for health-associated taxa, including _Corynebacterium_ and _Dolosigranulum_ spp.
In conclusion, there appear to be clear patterns of microbiota seeding from mother to child across multiple niches. Our data also suggest auxiliary routes of mother-to-infant seeding that may compensate for one another, ensuring essential microbes or microbial functions are transferred irrespective of disrupted transmission routes, such as following a CS.

## Corresponding manuscript

As of the moment of writing, the corresponding manuscript is in preparation and will be submitted soon.
Once accepted a link to the full text paper will be added here.

## Related literature

This project includes samples from the Microbiome Utrecht Infant Study (MUIS). In this study, we longitudinally included infants, collecting samples at regular time intervals from birth on.

The Bogaert lab published four articles on the MUIS-cohort (see overview below) and at the moment of writing few are in preparation.

First author(s) | Year | Title | Journal
--- | --- | --- | ---
de Steenhuijsen Piters _et al._ | 2022 | [Early-life viral infections are associated with disadvantageous immune and microbiota profiles and recurrent respiratory infections](https://www.nature.com/articles/s41564-021-01043-2) | _Nat Microbiol_
Reyman & Clerc _et al._ | 2021 | [Microbial community networks across body sites are associated with susceptibility to respiratory infections in infants](https://www.nature.com/articles/s42003-021-02755-1) | _Commun biol_
Reyman _et al._ | 2019 | [Impact of delivery mode-associated gut microbiota dynamics on health in the first year of life](https://www.nature.com/articles/s41467-019-13014-7) | _Nat Comm_
Man & Clerc _et al._ | 2019 | [Loss of Microbial Topography between Oral and Nasopharyngeal Microbiota and Development of Respiratory Infections Early in Life](https://www.atsjournals.org/doi/full/10.1164/rccm.201810-1993OC) | _Am J Resp Crit Care Med_
Bosch & de Steenhuijsen Piters _et al._ | 2017 | [Maturation of the Infant Respiratory Microbiota, Environmental Drivers, and Health Consequences. A Prospective Cohort Study](https://www.atsjournals.org/doi/full/10.1164/rccm.201703-0554OC) | _Am J Resp Crit Care Med_
Bosch _et al._ | 2016 | [Development of Upper Respiratory Tract Microbiota in Infancy is Affected by Mode of Delivery](https://www.thelancet.com/journals/ebiom/article/PIIS2352-3964(16)30225-0/fulltext) | _EBioMedicine_

## Contact

* Wouter A.A. de Steenhuijsen Piters, Ph.D.
    * Email: [w.a.a.desteenhuijsenpiters@umcutrecht.nl](mailto:w.a.a.desteenhuijsenpiters@umcutrecht.nl)
    * Institution: University Medical Center Utrecht/National Institute for Public Health and the Environment
    * Department: Paediatric Infectious Disease and Immunology
    * Lab: Bogaert Lab; PI: Professor Debby Bogaert; [d.bogaert@ed.ac.uk](mailto:d.bogaert@ed.ac.uk)

## Acknowledgements

This poster is part of the [#betterposter](https://twitter.com/hashtag/betterposter?src=hashtag_click) movement started by [Mike Morrison](https://twitter.com/mikemorrison?lang=en).
