
This repository provides additional details to the scientific [poster](2020/Hinxton/poster.pdf) presented at [Exploring Human Host-Microbiome Interactions in Health and Disease](https://coursesandconferences.wellcomegenomecampus.org/our-events/exploring-human-host-microbiome-interactions-2020/) 2020 (see below).

## Full author list

Wouter A.A. de Steenhuijsen Piters <a href="https://orcid.org/0000-0002-1144-4067" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>1,2</sup>, Rebecca Watson<sup>3</sup>, Raiza Hasrat<sup>1,2</sup>, Kayleigh Arp<sup>1,2</sup>, Mei Ling J.N. Chu<sup>1,2</sup>, Pieter-Kees C. M. de Groot<sup>4</sup>, Marlies A. Van Houten<sup>5</sup>, Elisabeth A.M. Sanders<sup>2</sup> and [Debby Bogaert](https://www.ed.ac.uk/inflammation-research/people/principal-investigators/professor-debby-bogaert) <a href="https://orcid.org/0000-0003-2651-7000" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a><sup>1-3§</sup>

<sup>§</sup> Corresponding author.

## Affiliations

<sup>1</sup> Department of Paediatric Immunology and Infectious Diseases, Wilhelmina Children’s Hospital/University Medical Center Utrecht, Utrecht, The Netherlands.  
<sup>2</sup> National Institute for Public Health and the Environment, Bilthoven, The Netherlands.  
<sup>3</sup> University of Edinburgh Centre for Inflammation Research, Queen's Medical Research Institute, University of Edinburgh, Edinburgh, United Kingdom.  
<sup>4</sup> Department of Obstetrics and Gynaecology, Spaarne Gasthuis, Hoofddorp, the Netherlands.  
<sup>5</sup> Spaarne Gasthuis Academy, Hoofddorp and Haarlem, The Netherlands.

## Poster

![Poster](poster.png)

## Poster components explained



### Pre-processing

After quality filtering gene expression data (_arrayQualityMetrics_ R-package), we used robust multichip averaging (RMA) background correction and performed quantile normalization (_oligo_ R-package). Resulting raw expression values were log<sub>2</sub>-transformed for downstream analyses.

### Module activity over time

<img align="right" width="300" height="178" src="zoom/zoom_line.png">

Modules were generated through gene co-expression network analysis using the _CEMiTool_ R-package ([Russo](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2053-1) _et al._, BMC Bioinformatics, 2018). Genes were included based on the default unsupervised filtering method implemented in _CEMiTool_ (resulting in _n_ = 1,580 genes).

'Module activity' refers to gene set enrichment analysis (GSEA) results (_fgsea_ R-package), which was used to compare module genes to a ranked gene list with all genes. The ranked gene list was generated based on the mean gene expression value per time point (highest expressed genes first, lowest genes last). This GSEA-analysis results in normalized enrichment scores (NES) and these scores are plotted here on the *y*-axis (tranlated to 'module activity' for readability).

The analysis shows three large groups of genes with differential enrichment patterns over time. Especially module M1 and module M3 increase with age, whereas module M2 activity decreases with age.

### Differentially expressed genes

<img align="right" width="300" height="178" src="zoom/zoom_DEG.png">

We also assessed at what moment gene expression dynamics would be largest. To do so, we performed _limma_-analysis, comparing each given time point to its preceding time point. We detected the largest number of differentially expressed genes very early in life, over the first few days/first week. Particularly, we found overexpression of genes involved in (bacterial) pattern recognition pathways (Toll-like recepters [TLRs] and inflammasome signaling at day 1 compared to the day of birth. This may suggest early host-microbial crosstalk at the mucosal barrier right after birth. Given the surge of microbes the newborn is exposed to, this may not come as a surprise.

### Annotating modules

<img align="right" width="300" height="178" src="zoom/zoom_M.png">

Gene modules were subjected to overrepresentation analysis (ORA) using curated Reactome Pathway Database gene sets in order to get an idea of their biological function.

We found that especially module M1 and M3 were associated with immune pathways. Module M1 showed overrepresentation of innate immunity and neutrophil degranulation and module M3 was related to interferen signaling pathways (most known for its role in antiviral immunity).
Contrarily, module M2, consisting of a large number of genes, appeared to be associated with epithelial maturation and maintenance, among others showing enrichment for cilium assembly/function.

### Module activity and respiratory tract infection (RTI) susceptibility

<img align="right" width="300" height="178" src="zoom/zoom_RTI.png">

To determine the association between module activity (per-sample NES-scores) and RTI-susceptibility, we regressed NES-scores against the number of RTIs infants experienced over the first year of life (comparing infants with 0-2 RTIs [healthy reference subcohort] vs those with 3-4/5-7 or 3-7 RTIs).
These regression were performed using smoothing splines ANOVA (_metagenomeSeq_ R-package). We found that module M3 is significantly associated with the number of RTIs an infant eventually develops from birth on, whereas the postive relationship between module M1 and RTI susceptibility was found from the age of ~1.5 months and on. Conversely, module M2 was related to respiratory health.

### Relationship modules - microbiota/viral infection

<img align="right" width="300" height="178" src="zoom/zoom_micro.png">

#### Viral infection

Given the importance of interferon pathways in our response to viral infections, we first assessed the association between cumulative module M3 expression over the first 3 months of life and the age at which a virus could be detected for the first time. We found that higher cumulative M3-activity is related to an earlier time-to-first virus (survival analyses). Given that the first viral infections could already be identified at very young age (from week 1 and on), and that interferon is more likely a response to viral infection than _vice versa_, it could well be that first viral infections are the 'first hit' in triggering an interferon response.
Intriguingly, parents only reported respiratory symptoms at much later age, despite these early dynamics in viral colonization/infection and mucosal immune responses.

#### Microbiota

We next asked ourselves how host gene expression profiles would impact (timing of) microbial dynamics. We assessed this with survival analyses, showing that early module M3 activity is related to premature _Moraxella_ spp. colonization, which has been shown to be associated with higher RTI susceptibility in earlier papers from our group (see below) and others. Cumulative M3 activity was also related to early _Haemophilus_ predominance, which is well known in the respiratory microbiota field for its association with inflammatory diseases.

Not shown on the poster, but to confirm our module-based findings, we ran a [HAllA](https://huttenhower.sph.harvard.edu/halla)-analysis, to assess age- and subject-indepedent correlations between genes and microbiota members. We identified a group of genes that were positively related to _Haemophilus_ spp. and negatively related to other commensals (among others _Corynebacterium_/_Dolosigranulum_ spp.). These genes were strongly related to innate immune pathways, among others neutrophil activation/signaling, verifying previous results. More importantly, even after removing age-/subject-driven effects, we could detect microbe-specific immune signatures in these infants, suggesting early-life host-microbial crosstalk.

## Corresponding manuscript

As of the moment of writing, the corresponding manuscript is in preparation and will be submitted soon.
Once accepted a link to the full text paper will be added here.

## Related literature

This project includes samples from the Microbiome Utrecht Infant Study (MUIS). In this study, we longitudinally included infants, collecting samples at regular time intervals from birth on.

The Bogaert lab published four articles on the MUIS-cohort (see overview below) and at the moment of writing few are in preparation.

First author(s) | Year | Title | Journal
--- | --- | --- | ---
Reyman _et al._ | 2019 | [Impact of delivery mode-associated gut microbiota dynamics on health in the first year of life](https://www.nature.com/articles/s41467-019-13014-7) | _Nat Comm_
Man & Clerc _et al._ | 2019 | [Loss of Microbial Topography between Oral and Nasopharyngeal Microbiota and Development of Respiratory Infections Early in Life](https://www.atsjournals.org/doi/full/10.1164/rccm.201810-1993OC) | _Am J Resp Crit Care Med_
Bosch & de Steenhuijsen Piters _et al._ | 2017 | [Maturation of the Infant Respiratory Microbiota, Environmental Drivers, and Health Consequences. A Prospective Cohort Study](https://www.atsjournals.org/doi/full/10.1164/rccm.201703-0554OC) | _Am J Resp Crit Care Med_
Bosch _et al._ | 2016 | [Development of Upper Respiratory Tract Microbiota in Infancy is Affected by Mode of Delivery](https://www.thelancet.com/journals/ebiom/article/PIIS2352-3964(16)30225-0/fulltext) | _EBioMedicine_

## Contact

* Wouter A.A. de Steenhuijsen Piters, Ph.D.
    * Email: [w.a.a.desteenhuijsenpiters@umcutrecht.nl](mailto:w.a.a.desteenhuijsenpiters@umcutrecht.nl)
    * Institution: University Medical Center Utrecht/National Institute for Public Health and the Environment
    * Department: Paediatric Infectious Disease and Immunology
    * Lab: Bogaert Lab; PI: Professor Debby Bogaert; [d.bogaert@ed.ac.uk](mailto:d.bogaert@ed.ac.uk)

## Acknowledgements

This poster is part of the [#betterposter](https://twitter.com/hashtag/betterposter?src=hashtag_click) movement started by [Mike Morrison](https://twitter.com/mikemorrison?lang=en).
