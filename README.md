Scientific posters 
===================================

Author: _Wouter A.A. de Steenhuijsen Piters_.

This repository keeps my scientific posters.

## Overview

Year | Event | Location | Title
--- | --- | --- | --- 
[2022](2022/) | [Exploring Human Host-Microbiome Interactions in Health and Disease](https://coursesandconferences.wellcomeconnectingscience.org/event/microbiome-interactions-in-health-and-disease-20221024/) | Hinxton, United Kingdom | [Mother-infant microbiota transmission and infant microbiota development across multiple body sites](2022/Hinxton)
[2021](2021/) | [International Human Microbiome Consortium Congress](https://ihmc2021.com) | Barcelona/Virtual Congress | [Early-life host-microbiota crosstalk in the upper airways is related to respiratory infection susceptibility](2021/IHMC)
[2020](2020/) | [Exploring Human Host-Microbiome Interactions in Health and Disease](https://coursesandconferences.wellcomegenomecampus.org/our-events/exploring-human-host-microbiome-interactions-2020/) | Hinxton, United Kingdom | [Early-life host-microbiota crosstalk in the upper airways is related to respiratory health](2020/Hinxton)
[2016](2016/) | [EMBO Next Generation Immunology](https://www.weizmann.ac.il/conferences/NGI2016/welcome) | Rehovot, Israel | [Nasopharyngeal microbiota, host transcriptome and respiratory syncytial virus disease severity](2016/EMBO_Next_Gen_Immuno)
[2016](2016/)| [International Symposium On Pneumococci & Pneumococcal Diseases](https://isppd2016.kenes.com) | Glasgow, Scotland | [Nasopharyngeal microbiota, especially _Streptococcus_- and _H. influenzae_-dominated profiles, are associated with inflammation and disease severity in children with respiratory syncytial virus infection](2016/EMBO_Next_Gen_Immuno)
[2014](2014/) | International Symposium On Pneumococci & Pneumococcal Diseases | Hyderabad, India | [Dysbiosis of upper respiratory tract microbiota in elderly pneumonia patients](2014/ISPPD)

For an overview of scientific presentations please see [here](https://gitlab.com/wsteenhu/presentations).

## License

All posters are licensed with [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/).